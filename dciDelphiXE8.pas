unit dciDelphiXE8;

interface

uses Windows, System.Classes, System.SysUtils, dciDelphi, dciRegistry;

type
  TDelphiXE8 = class(TDelphi)
  private

  public
    procedure Install; override;
  end;

implementation

{ TDelphiXE8 }

procedure TDelphiXE8.Install;
var
  I: Integer;
begin
  inherited;
  for I := 0 to (Options.DesignPackages.Count -1) do
    AddName(HKEY_CURRENT_USER, 'Software\Embarcadero\'+Options.Instance+'\16.0\Known Packages',IncludeTrailingPathDelimiter(Options.LibraryPath)+Options.DesignPackages[I], Options.Descriptions[I]);
  if Options.AddLibraryPath then
    UpdateValue(HKEY_CURRENT_USER, 'Software\Embarcadero\'+Options.Instance+'\16.0\Library\Win32','Search Path', Options.LibraryPath);
  if Options.AddEnvironmentVariables then
    UpdateValue(HKEY_CURRENT_USER, 'Software\Embarcadero\'+Options.Instance+'\16.0\Environment Variables','Path', Options.LibraryPath, Options.AddBefore);
end;

end.
