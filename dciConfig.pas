unit dciConfig;

interface

uses System.Classes, System.SysUtils, Inifiles,
  dciDelphi, dciDelphiXE8, dciDelphi7, dciDelphi5;

procedure RunInstall(const IniFile: string);

implementation

{Compiler	CompilerVersion Defined Symbol

Delphi 10 Seattle 30  VER300
Delphi XE8	      29	VER290
Delphi XE7	      28	VER280
Delphi XE6	      27	VER270
AppMethod 1	    26.5	VER265
Delphi XE5	      26	VER260
Delphi XE4	      25	VER250
Delphi XE3	      24	VER240
Delphi XE2	      23	VER230
Delphi XE	        22	VER220
Delphi 2010	      21	VER210
Delphi 2009	      20	VER200
Delphi 2007 .NET	19	VER190
Delphi 2007	    18.5	VER185
Delphi 2006	      18	VER180
Delphi 2005	      17	VER170
Delphi 8 .NET	    16	VER160
Delphi 7	        15	VER150
Delphi 6	        14	VER140
Delphi 5	        13	VER130
Delphi 4	        12	VER120
Delphi 3	        10	VER100
Delphi 2	         9	VER90
Delphi 1	         8	VER80
}

const
  cvXE8 = 'VER290';
  cv7   = 'VER150';
  cv5   = 'VER130';

procedure RunInstallDelphiXE8(Section: TStrings);
var
  Delphi: TDelphiXE8;
begin
  Delphi := TDelphiXE8.Create(Section);
  try
    Delphi.Install;
  finally
    Delphi.Free;
  end;
end;

procedure RunInstallDelphi7(Section: TStrings);
var
  Delphi: TDelphi7;
begin
  Delphi := TDelphi7.Create(Section);
  try
    Delphi.Install;
  finally
    Delphi.Free;
  end;
end;

procedure RunInstallDelphi5(Section: TStrings);
var
  Delphi: TDelphi5;
begin
  Delphi := TDelphi5.Create(Section);
  try
    Delphi.Install;
  finally
    Delphi.Free;
  end;
end;

function GetVersion(Section: TStrings): string;
var
  I: Integer;
begin
  for I := 0 to Section.Count -1 do
    if Section.Names[I].Equals('DelphiVersion') then
    begin
      Result := Section.ValueFromIndex[I];
      Exit;
    end;
end;

procedure RunInstall(const IniFile: string);
var
  I: Integer;
  Ini: TMemIniFile;
  Sections: TStringList;
  Section: TStringList;
  Version: string;
begin
  Ini := TMemIniFile.Create(IniFile);
  Sections := TStringList.Create;
  Section := TStringList.Create;
  try
    Ini.ReadSections(Sections);
    for I := 0 to Sections.Count -1 do
    begin
      Section.Clear;
      Ini.ReadSectionValues(Sections[I], Section);
      Version := GetVersion(Section);
      if Version.Equals(cvXE8) then
        RunInstallDelphiXE8(Section)
      else if Version.Equals(cv5) then
        RunInstallDelphi5(Section)
      else
        Writeln('ERROR: Unknown Delphi Version "'+Version+'".');
        Writeln('Known Delphi Versions:');
        Writeln('Delphi 5   - VER130');
        Writeln('Delphi 7   - VER150');
        Writeln('Delphi XE8 - VER290');
    end;
  finally
    Sections.Free;
    Section.Free;
    Ini.Free;
  end;
end;

end.
