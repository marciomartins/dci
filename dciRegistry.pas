unit dciRegistry;

interface

uses Windows, Registry, Vcl.Dialogs, System.SysUtils;

procedure UpdateValue(RootKey: HKEY; Key, Name, Value: string; Before: Boolean = False);
procedure AddName(RootKey: HKEY; Key, Name, Value: string);

implementation

procedure UpdateValue(RootKey: HKEY; Key, Name, Value: string; Before: Boolean);
var
  reg        : TRegistry;
  openResult : Boolean;
  CurrentValue: string;
begin
  reg := TRegistry.Create(KEY_READ);
  try
    reg.RootKey := RootKey;

    if (not reg.KeyExists(Key)) then
      raise Exception.Create('Key not found.');

    reg.Access := KEY_ALL_ACCESS;
    openResult := reg.OpenKey(Key,False);
    CurrentValue := reg.ReadString(Name);

    if not openResult then
      raise Exception.Create('Unable to open key.');
    if not CurrentValue.Contains(Value) then
      if Before then
        reg.WriteString(Name, Value+';'+CurrentValue)
      else
        reg.WriteString(Name, CurrentValue+';'+Value);
  finally
    reg.CloseKey();
    reg.Free;
  end;
end;

procedure AddName(RootKey: HKEY; Key, Name, Value: string);
var
  reg        : TRegistry;
  openResult : Boolean;
  CurrentValue: string;
begin
  reg := TRegistry.Create(KEY_READ);
  try
    reg.RootKey := RootKey;

    if (not reg.KeyExists(Key)) then
      raise Exception.Create('Key not found.');

    reg.Access := KEY_ALL_ACCESS;
    openResult := reg.OpenKey(Key,True);
    CurrentValue := reg.ReadString(Name);

    if not openResult then
      raise Exception.Create('Unable to open key.');
    reg.WriteString(Name, Value);
  finally
    reg.CloseKey();
    reg.Free;
  end;
end;

end.
