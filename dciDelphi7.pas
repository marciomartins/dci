unit dciDelphi7;

interface

uses Windows, System.Classes, System.SysUtils, dciDelphi, dciRegistry;

type
  TDelphi7 = class(TDelphi)
  private

  public
    procedure Install; override;
  end;

implementation

{ TDelphiXE8 }

procedure TDelphi7.Install;
var
  I: Integer;
begin
  inherited;
  for I := 0 to Options.DesignPackages.Count -1 do
    AddName(HKEY_CURRENT_USER, '\Software\Borland\'+Options.Instance+'\7.0\Known Packages',IncludeTrailingPathDelimiter(Options.LibraryPath)+Options.DesignPackages[I], Options.Descriptions[I]);
  if Options.AddLibraryPath then
    UpdateValue(HKEY_CURRENT_USER, 'Software\Borland\'+Options.Instance+'\7.0\Library','Search Path', Options.LibraryPath);
  if Options.AddBrowsingPath then
    UpdateValue(HKEY_CURRENT_USER, 'Software\Borland\'+Options.Instance+'\7.0\Library','Browsing Path', Options.LibraryPath);
  if Options.AddEnvironmentVariables then
    UpdateValue(HKEY_CURRENT_USER, 'Software\Borland\'+Options.Instance+'\7.0\Environment Variables','Path', Options.LibraryPath);
end;

end.
