unit dciDelphi5;

interface

uses Windows, System.Classes, System.SysUtils, dciDelphi, dciRegistry;

type
  TDelphi5 = class(TDelphi)
  private

  public
    procedure Install; override;
  end;

implementation

{ TDelphiXE8 }

procedure TDelphi5.Install;
var
  I: Integer;
begin
  inherited;
  for I := 0 to Options.DesignPackages.Count -1 do
    AddName(HKEY_CURRENT_USER, '\Software\Borland\'+Options.Instance+'\5.0\Known Packages',IncludeTrailingPathDelimiter(Options.LibraryPath)+Options.DesignPackages[I], Options.Descriptions[I]);
  if Options.AddLibraryPath then
    UpdateValue(HKEY_CURRENT_USER, 'Software\Borland\'+Options.Instance+'\5.0\Library','Search Path', Options.LibraryPath);
  if Options.AddEnvironmentVariables then
    SetEnvironmentVariable(PWideChar('Path'), PWideChar(Options.LibraryPath));
end;


end.
