unit dciDelphi;

interface

uses Windows, System.Classes, System.SysUtils;

const
  cnInstance = 'Instance';
  cnDescription = 'Descriptions';
  cnLibraryPath = 'LibraryPath';
  cnDesignPackages = 'DesignPackages';
  cnAddLibraryPath = 'AddLibraryPath';
  cnAddBrowsingPath = 'AddBrowsingPath';
  cnAddEnvironmentVariables = 'AddEnvironmentVariables';
  cnAddBefore = 'AddBefore';

type
  TDelphiOptions = class
  private
    FInstance: string;
    FDescriptions: TStrings;
    FLibraryPath: string;
    FDesignPackages: TStrings;
    FAddLibraryPath: Boolean;
    FAddEnvironmentVariables: Boolean;
    FAddBefore: Boolean;
    FAddBrowsingPath: Boolean;
    procedure SetValue(Name, Value: string);
  public
    constructor Create(Section: TStrings);
    destructor Destroy; override;
    property Instance: string read FInstance write FInstance;
    property Descriptions: TStrings read FDescriptions write FDescriptions;
    property DesignPackages: TStrings read FDesignPackages write FDesignPackages;
    property LibraryPath: string  read FLibraryPath write FLibraryPath;
    property AddLibraryPath: Boolean read FAddLibraryPath write FAddLibraryPath;
    property AddBrowsingPath: Boolean read FAddBrowsingPath write FAddBrowsingPath;
    property AddEnvironmentVariables: Boolean read FAddEnvironmentVariables write FAddEnvironmentVariables;
    property AddBefore: Boolean read FAddBefore write FAddBefore;
  end;

type
  TDelphi = class
  private
    FOptions: TDelphiOptions;
  protected
    function GetHKEY: HKEY; virtual;
  public
    constructor Create(Section: TStrings);
    destructor Destroy; override;
    property Options: TDelphiOptions read FOptions write FOptions;
    procedure Install; virtual; abstract;
  end;

implementation

{ TDelphiOptions }

constructor TDelphiOptions.Create(Section: TStrings);
var
  I: Integer;
begin
  FDescriptions := TStringList.Create;
  FDesignPackages := TStringList.Create;
  for I := 0 to Section.Count -1 do
    SetValue(Section.Names[I], Section.ValueFromIndex[I]);
end;

destructor TDelphiOptions.Destroy;
begin
  FDescriptions.Free;
  FDesignPackages.Free;
  inherited;
end;

procedure TDelphiOptions.SetValue(Name, Value: string);
begin
  if Name.Equals(cnInstance) then
    FInstance := Value
  else if Name.Equals(cnDescription) then
  begin
    FDescriptions.Delimiter := ';';
    FDescriptions.DelimitedText := Value;
  end
  else if Name.Equals(cnLibraryPath) then
    FLibraryPath := Value
  else if Name.Equals(cnDesignPackages) then
  begin
    FDesignPackages.Delimiter := ';';
    FDesignPackages.DelimitedText := Value;
  end
  else if Name.Equals(cnAddLibraryPath) then
    FAddLibraryPath := Value.Equals('True')
  else if Name.Equals(cnAddBrowsingPath) then
    FAddBrowsingPath := Value.Equals('True')
  else if Name.Equals(cnAddEnvironmentVariables) then
    FAddEnvironmentVariables := Value.Equals('True')
  else if Name.Equals(cnAddBefore) then
    FAddBefore := Value.Equals('True')
end;

{ TDelphi }

constructor TDelphi.Create(Section: TStrings);
begin
  FOptions := TDelphiOptions.Create(Section);
end;

destructor TDelphi.Destroy;
begin
  inherited;
end;

function TDelphi.GetHKEY: HKEY;
begin
  Result := HKEY_CURRENT_USER;
end;

end.
