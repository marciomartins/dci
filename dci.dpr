program dci;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  Windows,
  Registry,
  System.SysUtils,
  Vcl.Dialogs,
  dciRegistry in 'dciRegistry.pas',
  dciConfig in 'dciConfig.pas',
  dciDelphi in 'dciDelphi.pas',
  dciDelphiXE8 in 'dciDelphiXE8.pas',
  dciDelphi5 in 'dciDelphi5.pas',
  dciDelphi7 in 'dciDelphi7.pas';

var
  ConfigFile: string;

begin
  try
    if ParamCount <= 1 then
    begin
      Writeln('Sample to Use.');
      Writeln('Call without pause ');
      Writeln('dci.ext -f ConfigFile.ini');
      Writeln('Call with pause');
      Writeln('dci.ext -p -f ConfigFile.ini');
      Exit;
    end;
    if FindCmdLineSwitch('f', ConfigFile) then
      RunInstall(ConfigFile);
    if FindCmdLineSwitch('p') then
    begin
      Writeln('Press anny key to continue.');
      Readln;
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
